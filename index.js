"use strict";
const dbjs = require("./db.js");
exports.handler = async (event) => {
  const db = await dbjs.get();

  const data = await db
    .collection("hotstar-testAnalytics")
    .findOne({ "uuid._id": event.params.path.testUUID }, { uuid: 1, info: 1 });

  let kpiData;
  if(data.info.hasOwnProperty("performanceKpi")){
    kpiData=data.info.performanceKpi;
  }
  else{
    kpiData={}
  }

  let result = {
    version: "1.2",
    uuid: {
      _id: data.uuid._id,
      testId: data.uuid.testId,
      sessionId: data.uuid.testId,
      userDisplayName: data.uuid.userDisplayName,
      userEmail: data.uuid.userEmail,
      username: data.uuid.username,
    },
    performance: kpiData
  };

  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
