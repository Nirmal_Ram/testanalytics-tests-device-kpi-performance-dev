"use strict";
const MongoClient = require('mongodb').MongoClient;
const MONGODB_URI = "mongodb+srv://root:mozark22@cluster0.bqf81.mongodb.net/testAnalytics?retryWrites=true&w=majority";
let dbInstance = null;
module.exports.get = async function () {
  if (dbInstance) {
    return Promise.resolve(dbInstance);
  }
  const db = await MongoClient.connect(MONGODB_URI);
  dbInstance = db.db("testAnalytics");
  return dbInstance;
}